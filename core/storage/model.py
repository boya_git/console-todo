from abc import ABCMeta

from core.storage.abstract import AbstractModel
from core.storage.abstract import AbstractColumn


class MetaColumnsRegister(ABCMeta):

    def __new__(mcs, name, parents, class_dict):
        # _columns is the list of current class descriptors names
        # Copy parents _columns if exists
        parents_columns = [
            column
            for parent in parents
            for column in parent.__dict__.get('_columns', [])
        ]
        # Prevent Identical Field Conflict in Multiple Inheritance
        if len(set(parents_columns)) != len(parents_columns):
            raise AttributeError(f'Multiple columns with one name in parenst classes {parents}')
        # Create new class
        new_class = super().__new__(mcs, name, parents, class_dict)
        # Parents _columns as base
        columns = parents_columns
        # Extends by current class _columns
        columns.extend([
            column_name
            for column_name, column in new_class.__dict__.items()
            if isinstance(column, AbstractColumn)
        ])
        # Set new obj of _columns to new class to prevent parent _columns list object overriding
        setattr(new_class, '_columns', columns)
        return new_class


class Model(AbstractModel, metaclass=MetaColumnsRegister):

    def __init__(self):
        self._storage = dict().fromkeys(self._columns)

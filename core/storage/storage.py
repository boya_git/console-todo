from copy import deepcopy


class Storage:

    __objs = dict()
    __latest_id = None
    __used_ids = set()

    def __init__(self):
        print('Storage initition...')

    def add(self, obj):
        new_obj_id = self._create_new_id()
        obj.id = new_obj_id
        self.__objs[new_obj_id] = deepcopy(obj._storage)

    def remove(self, id: int):
        del self.__objs[id]

    def get(self, id: int):
        return self.__objs.get(id)

    def _create_new_id(self):
        if self.__latest_id is None:
            self.__latest_id = -1
        self.__latest_id += 1
        new_id = self.__latest_id
        self.__used_ids.add(new_id)
        return new_id

    def __len__(self):
        return len(self.__objs)

    def __iter__(self):
        yield from self.__objs.values()

from decimal import Decimal

from core.storage.model.interfaces import IColumn



class Column(IColumn):
    """ Can work only in Model class childs. """

    data_type = None
    name = None

    def __init__(self, validators: list = []) -> None:
        self.validators = validators

    def __set_name__(self, owner, name):
        self.name = name

    def __get__(self, obj, owner=None):
        if obj is None:
            return
        return obj._local_storage.get(self.name)

    def __set__(self, obj, value):
        self._validate(value)
        obj._local_storage[self.name] = value

    def _validate(self, value):
        if not isinstance(value, self.data_type):
            raise ValueError(f"""
                Variable type must be {self.data_type}. But: {type(value)}
            """)
        for validator in self.validators:
            validator.execute(value)
        return True

    def _cast_to_python_type(self, _value):
        return self.data_type(_value)



class BooleanColumn(Column):

    data_type = bool


class IntegerColumn(Column):

    data_type = bool


class IntegerColumn(Column):
    
    data_type = int


class DecimalColumn(Column):

    data_type = Decimal


class StringColumn(Column):

    data_type = str

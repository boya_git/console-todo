from abc import ABCMeta

from .interfaces import IModel
from .interfaces import IColumn


class MetaColumnsRegister(ABCMeta):

    def __new__(mcs, name, parents, class_dict):
        # _columns is the list of current class descriptors names
        # Copy parents _columns if exists, prevent duplicates
        parents_columns = set()
        for parent in parents:
            if not hasattr(parent, '_columns'):
                continue
            if duplicated := parents_columns & set(parent._columns):
                raise AttributeError(f'''
                    Duplicate columns detected! {parent}: {duplicated}
                ''')
            parents_columns.update(parent._columns)

        # Create new class
        new_class = super().__new__(mcs, name, parents, class_dict)

        # Parents _columns as base
        columns = parents_columns
        # Extends by current class _columns
        columns.update({
            column_name
            for column_name, column in new_class.__dict__.items()
            if isinstance(column, IColumn)
        })

        # Set new obj of _columns to new class to prevent parent _columns list
        # object overriding
        setattr(new_class, '_columns', columns)
        return new_class


class Model(IModel, metaclass=MetaColumnsRegister):

    def __init__(self):
        self._local_storage = dict().fromkeys(self._columns)

    def save(self): ...

    @classmethod    
    def get(cls, id: int): ...

    def delete(self): ...

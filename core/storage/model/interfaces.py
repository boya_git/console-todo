from abc import ABC
from abc import abstractmethod


class IModel(ABC):

    @abstractmethod
    def save(self): ...

    @classmethod
    @abstractmethod
    def get(cls, id: int): ...

    @abstractmethod
    def delete(self): ...


class IColumn(ABC):

    @property
    @abstractmethod
    def data_type(self): ...
